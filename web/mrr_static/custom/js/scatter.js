$(document).ready( function() {

	var matrix = [];
	var title = "Scatter Chart";
	var chart_4 = $("#sample-chart-4");

	$("#mrr-breakdown p").each(function() {
		// Pull in matrix from the front end
		// x value
		var x = $(this).attr("data-x-label");
		// y value
		var y = $(this).attr("data-new");
		// Add to matrix here
		matrix.push({x,y});
	});

	scatterGraph(chart_4, title, matrix);
});



/* Function: Create a graph out of the variables coming in */
/* Can handle mixed graphs.
   Parameters: chart_canvas: ID of where graph should be placed
   	           title: not used right now,
			   x_data: x-axis labeling
			   y_matrix: contains data, color, look, of the y-axis */
function scatterGraph(chart_canvas, title, matrix) {
  // Enter code below...
 var scatterChart = new Chart(chart_canvas, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'Scatter Dataset',
            data: matrix
        }]
    },
    options: {
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }
});
  // http://www.chartjs.org/docs/latest/charts/scatter.html
}


